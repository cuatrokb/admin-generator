# Admin Generator

Admin Generator is a package that helps you quickly scaffold your typical CRUD admin interfaces. It generates the code based on the existing (migrated) table in the database. It uses an admin UI template from our other package `cuatrokb/admin-ui`.

Example of an administration interface generated with this package:
![Admin Panel administration area example](https://docs.getcraftable.com/assets/posts-crud.png "Admin Panel administration area example")

This packages is part of [Admin Panel](https://github.com/cuatrokb/admin-panel) (`cuatrokb/admin-panel`) - an administration starter kit for Laravel 5. You should definitely have a look :)


## Documentation
You can find full documentation at https://docs.cuatrokb.com/admin-panel/#/admin-generator
