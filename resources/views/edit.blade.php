{{'@'}}extends('admin.layout.default')

{{'@'}}section('title', trans('admin.{{ $modelLangFormat }}.actions.edit', ['name' => ${{ $modelVariableName }}->{{$modelTitle}}]))

{{'@'}}section('body')

    <h4 class="font-weight-bold mb-4">
        {{'{{'}} trans('admin.{{ $modelLangFormat }}.title') }}
        <div class="text-muted text-tiny">
            <small class="font-weight-normal"></small>
        </div>
    </h4>
    <div class="container-xl">

        <div class="card">

            @if($hasTranslatable)<{{ $modelJSName }}-form
                :action="'{{'{{'}} ${{ $modelVariableName }}->resource_url }}'"
                :data="{{'{{'}} ${{ $modelVariableName }}->toJsonAllLocales() }}"
                :locales="@{{ json_encode($locales) }}"
                :send-empty-locales="false"
                inline-template>
            @else<{{ $modelJSName }}-form
                :action="'{{'{{'}} ${{ $modelVariableName }}->resource_url }}'"
                :data="{{'{{'}} ${{ $modelVariableName }}->toJson() }}"
                inline-template>
            @endif

                <form class="form-horizontal form-edit" method="post" {{'@'}}submit.prevent="onSubmit" :action="this.action" novalidate>
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                <i class="fa fa-pencil"></i> {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.edit', ['name' => ${{ $modelVariableName }}->{{$modelTitle}}]) }}
                            </div>
                            <div class="col-auto text-right">
                                <button type="button" class="btn btn-sm btn-outline-dark m-b-0" onclick="window.history.back()">
                                    <i class="fas fa-arrow-left"></i>
                                    @{{ trans('admin.btn.back') }}
                                </button>
                                <button type="submit" class="btn btn-sm btn-primary" :disabled="submiting">
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-save'"></i>
                                    @{{ trans('admin.btn.save') }}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        {{'@'}}include('admin.{{ $modelDotNotation }}.components.form-elements')
                    </div>
                </form>
        </{{ $modelJSName }}-form>
    </div>
</div>
{{'@'}}endsection
