{{'@'}}extends('admin.layout.default')

{{'@'}}section('title', trans('admin.{{ $modelLangFormat }}.actions.index'))

{{'@'}}section('body')
    <h4 class="font-weight-bold mb-4">
        {{'{{'}} trans('admin.{{ $modelLangFormat }}.title') }}
        <div class="text-muted text-tiny">
            <small class="font-weight-normal"></small>
        </div>
    </h4>
    <{{ $modelJSName }}-listing
        :data="{{'{{'}} $data->toJson() }}"
        :url="'{{'{{'}} url('admin/{{ $resource }}') }}'"
        inline-template>

        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.index') }}
@if($export)
                <a class="btn btn-primary pull-right m-b-0 ml-2" href="{{'{{'}} url('admin/{{ $resource }}/export') }}" role="button"><i class="fa fa-file-excel-o"></i>&nbsp; {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.export') }}</a>
@endif
                <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{'{{'}} url('admin/{{ $resource }}/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.create') }}</a>
            </div>
            <div class="card-body" v-cloak>
                <form @submit.prevent="">
                    <div class="row justify-content-md-between">
                        <div class="col col-lg-7 col-xl-5 form-group">
                            <div class="input-group">
                                <input class="form-control" placeholder="@{{ trans('admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; @{{ trans('admin.btn.search') }}</button>
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-auto form-group ">
                            <select class="form-control" v-model="pagination.state.per_page">
                                {{-- TODO extract these options into a config or smtn --}}
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="100">100</option>
                            </select>
                        </div>

                    </div>
                </form>

                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            @foreach($columns as $col)<th is='sortable' :column="'{{ $col['name'] }}'">{{'{{'}} trans('admin.{{ $modelLangFormat }}.columns.{{ $col['name'] }}') }}</th>
                            @endforeach

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in collection">
                            @foreach($columns as $col)@if($col['switch'])<td>
                                <label class="switch switch-3d switch-success">
                                    <input type="checkbox" class="switch-input" v-model="collection[index].{{ $col['name'] }}" @change="toggleSwitch(item.resource_url, '{{ $col['name'] }}', collection[index])">
                                    <span class="switch-slider"></span>
                                </label>
                            </td>
                            @else<td>{{'@{{'}} item.{{ $col['name'] }}{{ $col['filters'] }} }}</td>@endif

                            @endforeach

                            <td>
                                <div class="row no-gutters">
                                    <div class="col-auto mx-1">
                                        <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="@{{ trans('admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                    </div>
                                    <form class="col mx-1" @submit.prevent="deleteItem(item.resource_url)">
                                        <button type="submit" class="btn btn-sm btn-danger" title="@{{ trans('admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="row" v-if="pagination.state.total > 0">
                    <div class="col-sm">
                        <span class="pagination-caption">@{{ trans('admin.pagination.overview') }}</span>
                    </div>
                    <div class="col-sm-auto">
                        <pagination></pagination>
                    </div>
                </div>

                <div class="no-items-found" v-if="!collection.length > 0">
                    <i class="icon-magnifier"></i>
                    <h3>@{{ trans('admin.index.no_items') }}</h3>
                    <p>@{{ trans('admin.index.try_changing_items') }}</p>
                    <a class="btn btn-primary btn-spinner" href="{{'{{'}} url('admin/{{ $resource }}/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.create') }}</a>
                </div>
            </div>
        </div>
    </{{ $modelJSName }}-listing>

{{'@'}}endsection
