
/* {{ $resource }} admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->prefix('admin/{{ $resource }}')->group(function () {
    {!! str_pad("Route::get('/',", 30) !!}'{{ $controllerPartiallyFullName }}@index');
    {!! str_pad("Route::post('/',", 30) !!}'{{ $controllerPartiallyFullName }}@store');
    {!! str_pad("Route::get('/create',", 30) !!}'{{ $controllerPartiallyFullName }}@create');
    {!! str_pad("Route::get('/{".$modelVariableName."}/edit',", 30) !!}'{{ $controllerPartiallyFullName }}@edit')->name('admin/{{ $resource }}/edit');
    {!! str_pad("Route::post('/{".$modelVariableName."}',", 30) !!}'{{ $controllerPartiallyFullName }}@update')->name('admin/{{ $resource }}/update');
    {!! str_pad("Route::delete('/{".$modelVariableName."}',", 30) !!}'{{ $controllerPartiallyFullName }}@destroy')->name('admin/{{ $resource }}/destroy');
@if($export)
    {!! str_pad("Route::get('/export',", 30) !!}'{{ $controllerPartiallyFullName }}@export')->name('admin/{{ $resource }}/export');
@endif
});
